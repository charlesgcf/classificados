<?php 
require 'environment.php';
define('VERSION', '1.0.0');
$config = array();

if (ENVIRONMENT == 'development'){
    define('BASE_URL', 'http://localhost:8080/classificados/');
    $config['dbname'] = 'classificados' ;
    $config['host'] = 'localhost' ;
    $config['dbuser'] = 'root' ;
    $config['dbpass'] = '' ;
} else {
    //dados da hospedagem em produ��o
    define('BASE_URL', 'http://www...');
    $config['dbname'] = 'classificados' ;
    $config['host'] = 'localhost' ;
    $config['dbuser'] = 'root' ;
    $config['dbpass'] = '' ;
}

global $db;

try {
    $db = new PDO(
        "mysql:dbname=".$config['dbname'].";host=".$config['host'],
        $config['dbuser'],
        $config['dbpass']
    );
} catch ( PDOException $e) {
    echo "ERRO: ".$e->getMessage();
}

?>