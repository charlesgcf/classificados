<?php ?>

<html>
	<head>
		<title>Modelo MVC</title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css" >
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css" >
		
	</head>
	<body>
	
		<div class="navbar navbar-dark bg-primary">
			<div class="container-fluid">
				<div class="navbar-header">
					<a href="<?php echo BASE_URL; ?>" class="navbar-brand bg-primary">Classificados</a>				
				</div>
				<ul class="nav navbar-nav navbar-right">
					<?php if(isset($_SESSION['cLogin']) && !empty($_SESSION['cLogin'])):?>
						<li><a class="bg-primary" href="<?php echo BASE_URL; ?>anuncios">Meus Anuncios</a></li>
						<li><a class="bg-primary" href="<?php echo BASE_URL; ?>usuario/logout">Sair</a></li>
					<?php else: ?>
						<li><a class="bg-primary" href="<?php echo BASE_URL; ?>usuario/cadastrar">Cadastre-se</a></li>
						<li><a class="bg-primary open_modal" href="<?php echo BASE_URL; ?>usuario/login">Login</a></li>
					<?php endif; ?>
				</ul>
			</div>		
		</div>
		<div class="body">
			<?php $this->loadViewInTemplate($viewName, $viewDada); ?>
		</div>	
		<div class="modal_bg">
			<div class="modal_bg close_modal">
        		
        	</div>
        	<div class="modal_">
        		
        	</div>
        </div>
        
        <div class="footer">
        	<strong>VERSÃO: </strong><?php echo VERSION ;?>
        </div>
		
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.mask.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
	</body>
</html>