<div class="container-fluid">
    <?php 
    if($preenchido) {
        if($cadastrado) {
            ?>
    		<div class="alert alert-success">
    			<strong>Parabéns!</strong> Cadastrado com sucesso. 
    			<a href="login.php" class="alert-link">Faça o login agora</a>
    		</div>
    		<?php
    	} else {
    		?>
    		<div class="alert alert-warning">
    			Este usuário já existe! <a href="login.php" class="alert-link">Faça o login agora</a>
    		</div>
    		<?php
    	}
    } else {
    	?>
    	<div class="alert alert-warning">
    		Preencha todos os campos!
    	</div>
    	<?php
    }
    ?>
    <div class="row">
		<div class="form-group  col-sm-6">
    		<h1>Cadastre-se</h1>
    	</div>
        
        <form method="post" >
        	<div class="form-group col-sm-12">
        		<label for="nome">Nome:</label>
        		<input class="form-control" id="nome" type="text" name="nome">
        	</div>
        	<div class="form-group col-sm-12">
        		<label for="email">E-mail:</label>
        		<input class="form-control" id="email" type="email" name="email">
        	</div>
        	<div class="form-group  col-sm-6">
        		<label for="nome">Senha:</label>
        		<input class="form-control" id="senha" type="password" name="senha">
        	</div>
        	<div class="form-group  col-sm-6">
        		<label for="nome">Telefone:</label>
        		<input class="form-control" id="telefone" type="text" name="telefone">
        	</div>
        	<div class="form-group  col-sm-6">
        		<input class="btn btn-default" value="Cadastrar" type="submit" >
        	</div>
        	
        </form>
    </div>
</div>