<?php 
if ($logando != 'login'){
    if($logando) {
        ?>
    	<script type="text/javascript">window.location.href="./";</script>
    	<?php
    } else if($logando == false) {
    	?>
    	<div class="alert alert-danger">
    		Usuário e/ou Senha errados!
    	</div>
    	<?php
    }
}
?>
<h1>Login</h1>
    <form method="post" id="formLogin">
    	<div class="form-group">
    		<label for="email">E-mail:</label>
    		<input class="form-control" id="email" type="email" name="email">
    	</div>
    	<div class="form-group">
    		<label for="nome">Senha:</label>
    		<input class="form-control" id="senha" type="password" name="senha">
    	</div>
    	
    	<input class="btn btn-primary" value="Entrar" type="submit" >
</form>

<script>
$(function(){
    $('#formLogin').on('submit', function(e){
    	e.preventDefault();
    	var form = $('#formLogin')[0];
    	var data = new FormData(form);
    	var base_url = window.location.origin+window.location.pathname;
    	
    	$.ajax({
    		type: 'POST',
    		url:'/classificados/',
    		data:data,
    		contentType:false,
    		processData:false,
    		success:function(msg){
    			if(msg.length > 0){
        			alert(msg);
    			}else{
    				window.location.href=base_url;
            	}
    		}
    	});
    });
});
</script>