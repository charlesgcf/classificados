<?php 
class controller {
    
    public function loadWiew($viewName, $viewDada = array() ){
        extract($viewDada);
        require_once 'views/'.$viewName. '.php';
    }
    
    public function loadTemplate($viewName, $viewDada = array()){
        require_once 'views/template.php';
    }
    
    public function loadViewInTemplate($viewName, $viewDada = array()){
        extract($viewDada);
        require_once 'views/'.$viewName. '.php';
    }
}
?>