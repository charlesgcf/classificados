<?php 
class usuarioController extends controller{
    
    public function index(){
        $this->loadWiew('login', $dados);

	}
	
	public function cadastrar() {
	    $u = new Usuarios();
	    $dados = array(
	        'preenchido' => '',
	        'cadastrado' => ''
	    );
	    if(isset($_POST['nome']) && !empty($_POST['nome'])) {
	        $nome = addslashes($_POST['nome']);
	        $email = addslashes($_POST['email']);
	        $senha = $_POST['senha'];
	        $telefone = addslashes($_POST['telefone']);
	        
	        $dados['preenchido'] = (!empty($nome) && !empty($email) && !empty($senha));
	        if($dados['preenchido']){
	           $dados['cadastrado'] = $u->cadastrar($nome, $email, $senha, $telefone);
	        }
        }
       
       $this->loadTemplate('cadastrar', $dados);
	    
	}
	
	public function login(){
	    $u = new Usuarios();
	    $dados['logando'] = 'login';
	    if(isset($_POST['email']) && !empty($_POST['email'])) {
	        $email = addslashes($_POST['email']);
	        $senha = $_POST['senha'];
	        
	        $dados['logando'] = $u->login($email, $senha);
	       
	    }
	    $this->loadWiew('login', $dados);
	}
	
	public function logout(){
	    $_SESSION['cLogin'] = '';
	    header("location: ../");
	}
	    
}
?>