<?php 
session_start();
require 'config.php';

$auto = function($class){
    
    if(file_exists('controllers/'.$class.'.php')) {
        require 'controllers/'.$class.'.php';
    }
    else if (file_exists('models/'.$class.'.php')) {
        require 'models/'.$class.'.php';
    }
    else if (file_exists('core/'.$class.'.php')) {
        require 'core/'.$class.'.php';
    }
    
};

spl_autoload_register($auto);

$core = new Core();
$core->run();


?>