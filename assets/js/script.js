$( document ).ready(function() {
	
	console.log( "ready!" );
	
	$('#telefone').mask('(99) 99999-9999' );
	$('#telefone').blur(function(event) {
	   if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	      $('#telefone').mask('(99) 99999-9999' );
	   } else {
	      $('#telefone').mask('(99) 9999-9999' );
	   }
	});
	
});

$(function() {
	
	$('.open_modal').on('click', function(e){
		e.preventDefault();
		
		$('.modal_').html('Carregando...');
		$('.modal_bg').show();
		
		var link = $(this).attr('href');
		
		$.ajax({
			url:link,
			type:'POST',
			success:function(html){
				$('.modal_').html(html);
			}
		});
	});
	
	$('.close_modal').on('click', function(e){
		
		$('.modal_bg').hide();
	});
	
});



